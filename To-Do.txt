To-do for some time in the future:
    - Add new team members to website
    - Create a code of conduct page at the website
    - Create that post to get help translating Garuda Assistant
    - Fix icon descriptions of Garuda Gamer (https://gitlab.com/garuda-linux/applications/garuda-gamer/-/issues/1)
    - Fix missing keybindings in zsh/fish (https://gitlab.com/garuda-linux/themes-and-settings/settings/garuda-zsh-config/-/issues/1)
    - Get kde-git edition working
    - Introduce new Angular website with dedicated pages for each edition
